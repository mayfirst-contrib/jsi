/*
Jitsi Meet Simultaneous Interpretation (JSI) - enable simultaneous
interpretation for Jitsi Meet sessions.

Copyright (C) 2021 Jamie McClelland

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

function Jsi(config) {
  const url = new URL(window.location.href); 
  const domain = config.jitsimeet_url;
  const meetingElementSelector = config.meetingElementSelector || '#meet';
  const options = {
    roomName: url.pathname.substr(1),
    height: "100%",
    width: "100%",
    // Disable Deep Linking so on mobile phones, the browser won't prompt the
    // user to use the app (which won't show our interpretation buttons).
    // Also, call in numbers won't work - you will hear all voices with no
    // option to mute the interpreter, so disable the feature to advertisze
    // them.
    configOverwrite: { disableDeepLinking: true, disableInviteFunctions: true },
    parentNode: document.querySelector(meetingElementSelector)
  };

  // Define the pattern to search display name to determine who are the
  // interpreters. If a display name contains this string, they will be
  // considered an interpreter and will be silenced or un-silenced as necessary.
  const interpreter_pattern = config.interpreter_pattern || /interpret/i

  var debug_enabled = config.debug || false;

  // The Jitsi Meet object - used to manipulate the meeting.
  var api = null;

  // Initialize everything.
  function init() {
    // Both the app and the splash page are hidden by default. Unhide one of
    // them, depending on whether or not there is a room in the URL, when we
    // load.
    if (!url.pathname.substr(1)) {
      // Show splash page.
      document.getElementById('splash-page').style.display = 'block';

      // Generate random room name.
      var str = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz';
      room = '';
      for (i = 1; i <= 15; i++) {
        var char = Math.floor(Math.random() * str.length + 1);
        room += str.charAt(char)
      }
      document.getElementById('room').value = room;
      document.getElementById('launch').addEventListener("click", function() {
        window.location.href = "/" + room;
      });
    }
    else {
      // Show the app.
      document.getElementById('app').style.display = 'block';

      // Create the jitsi meet object.
      api = new JitsiMeetExternalAPI(domain, options);

      // Setup our Jitsi Meet listeners.
      api.addListener('participantJoined', participantJoined);
      api.addListener('participantLeft', participantLeft);
      api.addListener('displayNameChange', displayNameChange);
      api.addListener('audioMuteStatusChanged', audioMuteStatusChanged);
      api.addListener('readyToClose', readyToClose);
      api.addListener('endpointTextMessageReceived', endpointTextMessageReceived);
      api.addListener('videoConferenceJoined', videoConferenceJoined);

      // Setup our javascript event handers.
      // Desktop interface controls.
      document.getElementById("interpretation-volume").addEventListener("change", changeInterpretationVolume);
      document.getElementById("interpretation-volume-mobile").addEventListener("change", changeMobileInterpretationVolume);
      document.getElementById("interpretation-icon").addEventListener("click", setInterpretationOn);
      document.getElementById("live-icon").addEventListener("click", setLiveOn);
      document.getElementById("interpretation-desktop-label").addEventListener("click", setInterpretationOn);
      document.getElementById("live-desktop-label").addEventListener("click", setLiveOn);
      document.getElementById("display-mobile-version").addEventListener("click", displayMobileVersion);
      document.getElementById("slow-down-request").addEventListener("click", slowDownRequest);
      document.getElementById('app').addEventListener("mouseenter", mouseEnterApp);
      document.getElementById('app').addEventListener("mouseleave", mouseLeaveApp);


      // Mobile interface controls.
      document.getElementById("display-desktop-version").addEventListener("click", displayDesktopVersion);
      document.getElementById("interpretation-mobile-label").addEventListener("click", setInterpretationOn);
      document.getElementById("live-mobile-label").addEventListener("click", setLiveOn);
      document.getElementById("hangup").addEventListener("click", hangup);
      document.getElementById('mobile-mute-toggle').addEventListener("click", toggleMute);
      document.getElementById('set-name').addEventListener("click", setDisplayName);
      document.getElementById('show-name-input').addEventListener("click", showNameInput); 
      

      // Default to either desktop or mobile vesion based on super simple test.
      window.addEventListener("load", function() {
        var isMobile = navigator.userAgent.toLowerCase().match(/mobile/);
        if (isMobile) {
          displayMobileVersion();
        }
        else {
          displayDesktopVersion();
        }
      });
    }
  }

  function participantJoined(arg) {
    debug("Someone joined: ", arg);
    // FIXME: If we set the newly joined participants volume right away
    // it doesn't always work - it seems we have to give the video component
    // a few seconds to fully load. Try to adjust it immediately, then, as
    // a fail safe, adjust it again in 4 seconds.
    adjustParticipantVolume(arg.displayName, arg.id);
    setTimeout(adjustParticipantVolume, 4000, arg.displayName, arg.id);
    populateParticipantList();
  }

  function displayNameChange(arg) {
    debug("Someone changed their name: ", arg);
    var displayName = arg.displayName || arg.displayname;

    adjustParticipantVolume(displayName, arg.id);
    populateParticipantList();

    // If we changed our display name, then update the mobile
    // interface to match and toggle the slow down button.
    if (arg.id == api._myUserID) { 
      toggleSlowDownButton(displayName);
      document.getElementById('name-value').innerHTML = displayName;
      document.getElementById('name-input-field').value = displayName;
    }
  }

  function participantLeft(arg) {
    debug("Someone left: ", arg);
    populateParticipantList();
  }


  // We present our own mute/unmute icon in the mobile version.
  // Ensure it is up to date when the audio/mute status changes.
  function audioMuteStatusChanged(arg) {
    if (arg.muted) {
      // We are currently muted
      src = "img/mic-off.png";
    }
    else {
      src = "img/mic-on.png";
    }
    // Update visual cue in mobile version.
    document.getElementById('mobile-mute-toggle').src = src
  }

  // This event triggers when the local user joins the video conference.
  function videoConferenceJoined(arg) {
    debug("Video conference joined", arg);

    // Adjust volumes of participants already in the call.
    adjustAllParticipantsVolume();

    var displayName = arg.displayName || arg.displayname;

    // Only show slow down button if we are the interpreter.
    toggleSlowDownButton(displayName);

    // Set our username in the mobile version.
    document.getElementById('name-value').innerHTML = displayName;
    document.getElementById('name-input-field').value = displayName;

    // And the participant list in the mobile version.
    populateParticipantList();
  }

  // Our volume slider is a scale between 0 and 100. We have to
  // convert that to a float between 0 and 1 so it will work with
  // what Jitsi Meet expects.
  function getVolume() {
    var volume = {};
    var id = null;
    var base = document.getElementById('interpretation-volume').value;
    volume.live = base / 100;
    volume.interpreter = (100 - base) / 100;
    return volume;
  }

  function isInterpreter(displayName) {
    debug("Testing if interpreter: ", displayName);
    if (interpreter_pattern.test(displayName)) {
      debug("Is interpreter");
      return true;
    }
    else {
      debug("Is not interpreter");
      return false;
    }
  }

  function adjustParticipantVolume(displayName, participantId) {
    var volume = getVolume();
    debug("Adjusting participant volume for: ", displayName);
    debug("Volumes are: ", volume);
    if (isInterpreter(displayName)) {
      debug("Adjusting interpreter volume: ", displayName, volume.interpreter);
      api.executeCommand('setParticipantVolume', participantId, volume.interpreter);
    }
    else {
      debug("Adjusting non-interpreter volume: ", displayName, volume.live);
      api.executeCommand('setParticipantVolume', participantId, volume.live);
    }
  }
  function adjustAllParticipantsVolume() {
    // Reload list of participants.
    participants = api.getParticipantsInfo();
    participants.forEach(function(value, index, arr) {
      adjustParticipantVolume(value.displayName, value.participantId);
    });
  }

  // When you tap the mic button in the mobile version, we should toggle the
  // mute status.
  function toggleMute() { 
    debug("Toggling audio mute");
    api.executeCommand('toggleAudio');
  }

  // The participant list is only displayed on mobile.
  function populateParticipantList() {
    var participants = api.getParticipantsInfo();
    var participantsList = [];
    participants.forEach(function(value, index, arr) {
      participantsList.push(value.displayName);
    });
    document.getElementById('participants-list').innerHTML = participantsList.join(', ');
  }

  // We only handle setting display name in the mobile version.
  function setDisplayName() {
    var displayName = document.getElementById('name-input-field').value;
    api.executeCommand('displayName', displayName);
    document.getElementById('name-input').style.display = 'none';
    document.getElementById('name-display').style.display = 'block';
  }

  function displayMobileVersion() {
    // Hide the desktop interface, show mobile.
    document.getElementById('meet').style.display = 'none';
    document.getElementById('desktop-version').style.display = 'none';
    document.getElementById('mobile-version').style.display = 'block';
  }

  function displayDesktopVersion() {
    // Hide the mobile interface, show desktop.
    document.getElementById('mobile-version').style.display = 'none';
    document.getElementById('desktop-version').style.display = 'block';
    document.getElementById('meet').style.display = 'block';
  }

  function setInterpretationVolume(volume) {
    document.getElementById('interpretation-volume-mobile').value = volume;
    document.getElementById('interpretation-volume').value = volume;
    adjustAllParticipantsVolume();
  }

  function setInterpretationOn() {
    setInterpretationVolume(0);
  }

  function setLiveOn() {
    setInterpretationVolume(100);
  }
  function showNameInput() {
    document.getElementById('name-input').style.display = 'block';
    document.getElementById('name-display').style.display = 'none';
  }

  function changeInterpretationVolume() {
    // Whenever we change the interpretation volume slider,
    // we have to adjust the mobile intrepretation volume controls as well
    // so if they switch to mobile view they will be the same.
    var volume = document.getElementById('interpretation-volume').value;
    document.getElementById('interpretation-volume-mobile').value = volume;

    // Now update all participants' valume to reflect this change.
    adjustAllParticipantsVolume();
  }

  function changeMobileInterpretationVolume() {
    // Whenever we change the interpretation volume slider on the mobile version,
    // we have to adjust the "real" intrepretation volume controls.
    var volume = document.getElementById('interpretation-volume-mobile').value;
    document.getElementById('interpretation-volume').value = volume;

    // Now update all participants' valume to reflect this change.
    adjustAllParticipantsVolume();
  }

  function readyToClose() {
    debug("Ready to close");
    // Redirect to empty page.
    window.location.href = "/";
  }

  function hangup() {
    api.executeCommand('hangup');
  }

  function debug(msg, val = null) {
    if (debug_enabled) {
      console.debug(msg, val);
    }
  }

  function endpointTextMessageReceived(arg) {
    debug("Receiving iterpreter slow down request.", arg);
    document.getElementById('slow-down-alert').style.display = 'block'; 
    setTimeout(function() {
        document.getElementById('slow-down-alert').style.display = "none"; 
      }, 5000
    );
  }

  function slowDownRequest() {
    // Unfortunately, there is:
    // a) No way to know who the dominant speaker is since it might be the interpreter.
    // b) No way to send a text message to the group chat, only to individuals.
    //
    // So... we send the message to every individual.
    participants = api.getParticipantsInfo();
    participants.forEach(function(value, index, arr) {
      // Don't send the message to ourselves or we will get an exception.
      if (value.participantId != api._myUserID) {
        debug("Sending message");
        api.executeCommand('sendEndpointTextMessage', value.participantId, 'Please slow down for interpreter.');
      }
    });
  }

  // The slow down button should only display for interpreters.
  function toggleSlowDownButton(displayName) {
    debug("Toggling slow down button: ", displayName)
    if (isInterpreter(displayName)) {
      showSlowDownButton();
    }
    else {
      hideSlowDownButton();
    }
  }

  function showSlowDownButton() {
    document.getElementById('slow-down-request').style.display = 'block';
  }

  function hideSlowDownButton() {
    document.getElementById('slow-down-request').style.display = 'none';
  }

  function mouseEnterApp() {
    document.getElementById("desktop-version").style.transitionDelay = '0s';
    document.getElementById("desktop-version").style.top = '15px';
  }

  function mouseLeaveApp() {
    document.getElementById("desktop-version").style.transitionDelay = '1.25s';
    document.getElementById("desktop-version").style.top = '-150px';
  }

  // Initialize everthing.
  init();
}

